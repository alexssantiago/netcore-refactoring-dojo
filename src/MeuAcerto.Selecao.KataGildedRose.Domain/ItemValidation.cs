﻿using FluentValidation;

namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class ItemValidation : AbstractValidator<Item>
    {
        public ItemValidation()
        {
            RuleFor(i => i.Nome)
                .NotNull().WithMessage("O {PropertyName} do item precisa ser fornecido.");

            RuleFor(i => i.Qualidade)
                .GreaterThanOrEqualTo(0).WithMessage("A {PropertyName} do item não pode ser negativa.");

            When(i => i.Nome != "Dente do Tarrasque", () =>
            {
                RuleFor(i => i.Qualidade)
                    .LessThanOrEqualTo(50).WithMessage("A {PropertyName} de um item não pode ser maior que 50.");
            });

            When(i => i.Nome == "Dente do Tarrasque", () =>
            {
                RuleFor(i => i.Qualidade)
                    .Equal(80).WithMessage("O (Dente do Tarrasque) tem {PropertyName} imutável de 80.");
            });
        }
    }
}
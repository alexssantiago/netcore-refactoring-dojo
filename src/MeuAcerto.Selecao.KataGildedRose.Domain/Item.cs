﻿namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class Item
    {
        public string Nome { get; set; }

        public int PrazoParaVenda { get; set; }

        public int Qualidade { get; set; }
    }
}

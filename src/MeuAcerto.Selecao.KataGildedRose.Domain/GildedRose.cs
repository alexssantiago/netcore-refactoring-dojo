﻿using System.Collections.Generic;
using System.Linq;

namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class GildedRose : IGildedRose
    {
        private readonly IList<Item> _itens;
        public IReadOnlyCollection<Item> ObterItens => _itens.ToList();

        public static int MAX_QUALIDADE_ITEM => 50;
        public static int MIN_QUALIDADE_ITEM => 0;

        public GildedRose(IList<Item> itens)
        {
            _itens = itens;
        }

        public void AtualizarEstoque()
        {
            foreach (var item in _itens)
            {
                AtualizarQualidade(item);
                AtualizarPrazoParaVenda(item);

                if (item.PrazoParaVenda < 0) AtualizarQualidadePrazoParaVendaExpirado(item);
            }
        }

        private void AtualizarQualidade(Item item)
        {
            if (item.Nome != "Queijo Brie Envelhecido" && item.Nome != "Ingressos para o concerto do Turisas")
            {
                DiminuirQualidade(item);
            }
            else
            {
                AumentarQualidade(item);
            }
        }

        private void DiminuirQualidade(Item item)
        {
            if (item.Qualidade > MIN_QUALIDADE_ITEM)
            {
                if (item.Nome.Contains("Conjurado"))
                {
                    DiminuirQualidadeConjurado(item);
                    return;
                }

                if (item.Nome != "Dente do Tarrasque") DiminuirQualidadePorUnidade(item, 1);
            }
        }

        private void DiminuirQualidadeConjurado(Item item)
        {
            DiminuirQualidadePorUnidade(item, 2);
        }

        private void AtualizarPrazoParaVenda(Item item)
        {
            if (item.Nome != "Dente do Tarrasque") item.PrazoParaVenda -= 1;
        }

        public void AtualizarQualidadePrazoParaVendaExpirado(Item item)
        {
            if (item.Nome != "Queijo Brie Envelhecido")
            {
                if (item.Nome != "Ingressos para o concerto do Turisas")
                {
                    DiminuirQualidade(item);
                }
                else
                {
                    AtualizarQualidadeIngressosPrazoParaVendaExpirado(item);
                }
            }
            else
            {
                AumentarQualidade(item);
            }
        }

        private void AtualizarQualidadeIngressosPrazoParaVendaExpirado(Item item)
        {
            item.Qualidade = MIN_QUALIDADE_ITEM;
        }

        public void AumentarQualidade(Item item)
        {
            if (PossuiQualidadeValida(item))
            {
                if (item.Nome == "Ingressos para o concerto do Turisas")
                {
                    AumentarQualidadeItemEspecifico(item);
                    return;
                }
                if (item.Nome != "Dente do Tarrasque") AumentarQualidadePorUnidade(item, 1);
            }
        }

        public void AumentarQualidadeItemEspecifico(Item item)
        {
            if (item.PrazoParaVenda <= 5)
            {
                AumentarQualidadeMenorOuIgualCinco(item);
                return;
            }
            if (item.PrazoParaVenda <= 10) AumentarQualidadeMenorOuIgualDez(item);
            else AumentarQualidadePorUnidade(item, 1);
        }

        private bool PossuiQualidadeValida(Item item)
        {
            return item.Qualidade < MAX_QUALIDADE_ITEM;
        }

        private void AumentarQualidadeMenorOuIgualCinco(Item item)
        {
            AumentarQualidadePorUnidade(item, 3);
            ReajustarValorQualidade(item);
        }

        private void AumentarQualidadeMenorOuIgualDez(Item item)
        {
            AumentarQualidadePorUnidade(item, 2);
            ReajustarValorQualidade(item);
        }

        private void ReajustarValorQualidade(Item item)
        {
            if (item.Qualidade > MAX_QUALIDADE_ITEM) item.Qualidade = MAX_QUALIDADE_ITEM;
        }

        private void AumentarQualidadePorUnidade(Item item, int unidade)
        {
            item.Qualidade += unidade;
        }

        private void DiminuirQualidadePorUnidade(Item item, int unidade)
        {
            item.Qualidade -= unidade;
        }
    }
}
﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    [Trait("Categoria", "Domain - Item")]
    public class ItemTest
    {
        [Fact(DisplayName = "Novo Item Válido")]
        public void Item_NovoItem_DeveEstarValido()
        {
            //Arrange
            var item = new Item { Nome = "Queijo Brie Envelhecido", PrazoParaVenda = -5, Qualidade = 50 };

            //Act
            var validate = new ItemValidation().Validate(item);

            //Assert
            Assert.True(validate.IsValid);
            Assert.Equal(0, validate.Errors.Count);
        }
        
        [Fact(DisplayName = "Novo Item com Qualidade abaixo do permitido")]
        public void Item_QualidadeAbaixoDoPermitido_DeveSerInvalida()
        {
            //Arrange
            var item = new Item { Nome = "Elixir do Mangusto", PrazoParaVenda = -5, Qualidade = -15 };

            //Act
            var validate = new ItemValidation().Validate(item);

            //Assert
            Assert.False(validate.IsValid);
            Assert.Equal(1, validate.Errors.Count);
        }

        [Fact(DisplayName = "Novo Item com Qualidade acima do permitido")]
        public void Item_QualidadeAcimaDoPermitido_DeveSerInvalida()
        {
            //Arrange
            var item = new Item { Nome = "Elixir do Mangusto", PrazoParaVenda = -5, Qualidade = 95 };

            //Act
            var validate = new ItemValidation().Validate(item);

            //Assert
            Assert.False(validate.IsValid);
            Assert.Equal(1, validate.Errors.Count);
        }

        [Fact(DisplayName = "Novo Dente do Tarrasque Válido")]
        public void Item_NovoDenteDoTarrasque_DeveEstarValido()
        {
            //Arrange
            var item = new Item { Nome = "Dente do Tarrasque", Qualidade = 80 };

            //Act
            var validate = new ItemValidation().Validate(item);

            //Assert
            Assert.True(validate.IsValid);
            Assert.Equal(0, validate.Errors.Count);
        }

        [Fact(DisplayName = "Novo Dente do Tarrasque Inválido")]
        public void Item_NovoDenteDoTarrasque_DeveEstarInvalido()
        {
            //Arrange
            var item = new Item { Nome = "Dente do Tarrasque", PrazoParaVenda = -5, Qualidade = 50 };

            //Act
            var validate = new ItemValidation().Validate(item);

            //Assert
            Assert.False(validate.IsValid);
            Assert.Equal(1, validate.Errors.Count);
        }
    }
}
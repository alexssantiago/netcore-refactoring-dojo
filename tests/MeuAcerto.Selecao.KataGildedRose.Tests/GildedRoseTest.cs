﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using System.Linq;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    [Collection(nameof(ItemCollection))]
    [Trait("Categoria", "Domain - Gilded Rose")]
    public class GildedRoseTest
    {
        private readonly ItemTestsFixture _itemTestsFixture;

        public GildedRoseTest(ItemTestsFixture itemTestsFixture)
        {
            _itemTestsFixture = itemTestsFixture;
        }

        [Fact(DisplayName = "Atualizar Estoque de Item Lendário")]
        public void AtualizarEstoque_ItemLendario_NaoDeveAtualizarQualidade()
        {
            //Arrange
            var itens = _itemTestsFixture.GerarItensValidos();
            var gildedRose = new GildedRose(itens);
            bool predicate(Item i) => i.Nome == "Dente do Tarrasque" && i.Qualidade == 80;

            //Act
            gildedRose.AtualizarEstoque();

            //Assert
            Assert.Equal(itens.Where(predicate).Count(), gildedRose.ObterItens.Where(predicate).Count());
        }

        [Fact(DisplayName = "Aumentar Qualidade de Queijo Brie Envelhecido")]
        public void AumentarQualidade_QueijoBrieEnvelhecido_AumentaEmUmaUnidade()
        {
            //Arrange
            var itens = _itemTestsFixture.GerarItensValidos();
            var queijoBrie = new Item { Nome = "Queijo Brie Envelhecido", Qualidade = 0, PrazoParaVenda = 100 };
            var qualidadeItem = queijoBrie.Qualidade;
            itens.Add(queijoBrie);
            var gildedRose = new GildedRose(itens);

            //Act
            gildedRose.AumentarQualidade(queijoBrie);

            //Assert
            Assert.True(new ItemValidation().Validate(queijoBrie).IsValid);
            Assert.Equal(qualidadeItem + 1, queijoBrie.Qualidade);
        }

        [Fact(DisplayName = "Atualizar Qualidade de Queijo Brie em 2 unidades quando o prazo para venda é igual ou menor que 10")]
        public void AtualizarQualidadePrazoParaVendaExpirado_QueijoBrieEnvelhecido_AumentaEmDuasUnidadesQuandoPrazoParaVendaIgualOuMenorQueDez()
        {
            //Arrange
            var itens = _itemTestsFixture.GerarItensValidos();
            var queijoBrie = new Item { Nome = "Queijo Brie Envelhecido", Qualidade = 13, PrazoParaVenda = 7 };
            var qualidadeItem = queijoBrie.Qualidade;
            itens.Add(queijoBrie);
            var gildedRose = new GildedRose(itens);

            // Act
            gildedRose.AumentarQualidadeItemEspecifico(queijoBrie);

            //Assert
            Assert.True(new ItemValidation().Validate(queijoBrie).IsValid);
            Assert.Equal(qualidadeItem + 2, queijoBrie.Qualidade);
        }

        [Fact(DisplayName = "Atualizar Qualidade de Queijo Brie em 3 unidades quando o prazo para venda é igual ou menor que 5")]
        public void AtualizarQualidadePrazoParaVendaExpirado_QueijoBrieEnvelhecido_AumentaEmTresUnidadesQuandoPrazoParaVendaIgualOuMenorQueCinco()
        {
            //Arrange
            var itens = _itemTestsFixture.GerarItensValidos();
            var queijoBrie = new Item { Nome = "Queijo Brie Envelhecido", Qualidade = 9, PrazoParaVenda = 3 };
            var qualidadeItem = queijoBrie.Qualidade;
            itens.Add(queijoBrie);
            var gildedRose = new GildedRose(itens);

            // Act
            gildedRose.AumentarQualidadeItemEspecifico(queijoBrie);

            //Assert
            Assert.True(new ItemValidation().Validate(queijoBrie).IsValid);
            Assert.Equal(qualidadeItem + 3, queijoBrie.Qualidade);
        }

        [Fact(DisplayName = "Atualizar Qualidade de Ingressos à 0 quando o prazo para venda tiver passado")]
        public void AtualizarQualidadePrazoParaVendaExpirado_QueijoBrieEnvelhecido_VaiAZeroQuandoPrazoParaVendaExpirado()
        {
            //Arrange
            var itens = _itemTestsFixture.GerarItensValidos();
            var ingressos = new Item { Nome = "Ingressos para o concerto do Turisas", Qualidade = 5, PrazoParaVenda = -1 };
            itens.Add(ingressos);
            var gildedRose = new GildedRose(itens);

            // Act
            gildedRose.AtualizarQualidadePrazoParaVendaExpirado(ingressos);

            //Assert
            Assert.True(new ItemValidation().Validate(ingressos).IsValid);
            Assert.Equal(GildedRose.MIN_QUALIDADE_ITEM, ingressos.Qualidade);
        }

        [Fact(DisplayName = "Atualizar Estoque de Item Conjurado")]
        public void AtualizarEstoque_ItemConjurado_DiminuiQualidadeEmDuasUnidades()
        {
            //Arrange
            var itens = _itemTestsFixture.GerarItensValidos();
            var conjurado = new Item { Nome = "Anel Conjurado", PrazoParaVenda = 4, Qualidade = 7 };
            var qualidadeItem = conjurado.Qualidade;
            itens.Add(conjurado);
            var gildedRose = new GildedRose(itens);

            //Act
            gildedRose.AtualizarEstoque();

            //Assert
            Assert.Contains(itens, i => i.Nome.Contains("Conjurado"));
            Assert.Equal(qualidadeItem - 2, itens.FirstOrDefault(i => i.Nome == "Anel Conjurado").Qualidade);
        }
    }
}

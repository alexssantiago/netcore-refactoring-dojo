﻿using ApprovalTests;
using ApprovalTests.Reporters;
using System;
using System.IO;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    [UseReporter(typeof(DiffReporter))]
    [Trait("Categoria", "Approval Test")]
    public class ApprovalTest
    {
        [Fact(DisplayName = "Atualizar Estoque em Trinta Dias")]
        public void TrintaDias()
        {
            var fakeoutput = new StringBuilder();
            Console.SetOut(new StringWriter(fakeoutput));
            Console.SetIn(new StringReader("a\n"));

            Domain.Program.Main(new string[] { });
            var output = fakeoutput.ToString();

            Approvals.Verify(output);
        }
    }
}
